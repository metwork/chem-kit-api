# ChemKit API

Expose the [ChemKit](http://chem-kit.metwork.science/) Python package 
on a REST API powered by [FastAPI](https://fastapi.tiangolo.com/).

## Debug minikiube

```bash
kubectl get po -n kube-system
kubectl logs -n kube-system kube-proxy-ghs4x
kubectl logs -n kube-system kube-proxy-7v8w5 
sudo sysctl net/netfilter/nf_conntrack_max=1572864
minikube delete
minikube start --driver=docker
kubectl get po -n kube-system
kubectl create deployment chem-kit-api --image=yannbeauxis/chem-kit-api:0.1.9
kubectl get pods
kubectl expose deployment chem-kit-api --type=NodePort --port=8000
```